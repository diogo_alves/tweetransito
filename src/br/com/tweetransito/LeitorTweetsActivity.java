package br.com.tweetransito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

public class LeitorTweetsActivity extends Activity {

	private ListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leitor);
		final String termoDeBusca = getIntent().getStringExtra("TERMO_DE_BUSCA");
		listView = (ListView) findViewById(R.id.lista_tweets);

		new BuscaTweetsTask().execute(termoDeBusca);
	}

	private void popularListView(ArrayList<Tweet> lista){
		TweetAdapter  tweetAdapter = new TweetAdapter(this, R.layout.lista_tweets_adapter, lista);
		listView.setAdapter(tweetAdapter);
	}

	private class BuscaTweetsTask extends AsyncTask<String, Void, ArrayList<Tweet>>{
		
		private ProgressDialog progressDialog;
		private static final String URL_BASE = "https://api.twitter.com";
		private static final String URL_BUSCA = URL_BASE + "/1.1/statuses/user_timeline.json?screen_name=";
		private static final String URL_AUTH = URL_BASE + "/oauth2/token";
		private static final String CONSUMER_KEY = "ij6IhV9BMZQld2i3oIoVJg";
		private static final String CONSUMER_SECRET = "gqnCNWQLZ7Bi3ZcbRKe9PixsiGWepm6bGYV244f21Q";
		
		private String autenticarApp(){
			
			HttpURLConnection conexao = null;
			OutputStream os = null;
			BufferedReader br = null;
			StringBuilder sb = null;

			try {
				URL url = new URL(URL_AUTH);
				conexao = (HttpURLConnection) url.openConnection();
				conexao.setRequestMethod("POST");
				conexao.setDoOutput(true);

				String credenciaisAcesso = CONSUMER_KEY + ":" + CONSUMER_SECRET;
				String autorizacao = "Basic " + Base64.encodeToString(credenciaisAcesso.getBytes(), Base64.NO_WRAP);
				String parametro = "grant_type=client_credentials";
				
				conexao.addRequestProperty("Authorization", autorizacao);
				conexao.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
				conexao.connect();
				
				os = conexao.getOutputStream();
				os.write(parametro.getBytes());
				os.flush();
				os.close();
				
				br = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
				String linha;
				sb = new StringBuilder();
				
				while ((linha = br.readLine()) != null){            
					sb.append(linha);	
				}

				Log.i("Resposta conexão post", String.valueOf(conexao.getResponseCode()));
				Log.i("Resposta json-chave-autorizacao", sb.toString());


			} catch (Exception e) {
				Log.e("Erro POST", Log.getStackTraceString(e));
			}finally{
				if (conexao != null) {
					try {
						conexao.disconnect();
					} catch (Exception e2) {
						Log.e("Erro disconnect", Log.getStackTraceString(e2));
					}
				}
			}
			return sb.toString();
		}
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			progressDialog = new ProgressDialog(LeitorTweetsActivity.this);
			progressDialog.setMessage("Carregando tweets. Aguarde...");
			progressDialog.show();
		}

		@Override
		protected ArrayList<Tweet> doInBackground(String... param) {

			
			String termoDeBusca = param[0];
			ArrayList<Tweet> tweets = new ArrayList<Tweet>();
			HttpURLConnection conexao = null;
			BufferedReader br = null;

			try {
				URL url = new URL(URL_BUSCA + termoDeBusca);
				conexao = (HttpURLConnection) url.openConnection();
				conexao.setRequestMethod("GET");

				String jsonString = autenticarApp();
				JSONObject json = new JSONObject(jsonString);

				conexao.setRequestProperty("Authorization", json.getString("token_type") + " " +
					json.getString("access_token"));
				conexao.setRequestProperty("Content-Type", "application/json");
				
				Log.d("Resposta conexão get", String.valueOf(conexao.getResponseCode()));
				
				br = new BufferedReader(new InputStreamReader(conexao.getInputStream()));

				String linha;
				StringBuilder builder = new StringBuilder();
				JSONObject jsonObject;
				JSONArray jsonArray;
				while ((linha = br.readLine()) != null){            
					builder.append(linha);	
				}
				
				Log.i("Dados recebidos", builder.toString());
				jsonArray = new JSONArray(builder.toString());

				for (int i = 0; i < jsonArray.length(); i++) {
					jsonObject = (JSONObject) jsonArray.get(i);
					
					Tweet tweet = new Tweet();
					
					tweet.setNome(jsonObject.getJSONObject("user").getString("name"));
					tweet.setUsuario(jsonObject.getJSONObject("user").getString("screen_name"));
					tweet.setUrlImagemPerfil(jsonObject.getJSONObject("user").getString("profile_image_url"));
					tweet.setMensagem(jsonObject.getString("text"));
					tweet.setData(jsonObject.getString("created_at"));

					tweets.add(i, tweet);
				}

			} catch (MalformedURLException e) {
				Log.e("MalformedURLException: ", Log.getStackTraceString(e));        

			} catch (IOException e) {
				Log.e("IOException: ", Log.getStackTraceString(e));        

			} catch (JSONException e) {
				Log.e("JSONException: ", Log.getStackTraceString(e));   

			}finally {
				if(br != null){
					try{
						br.close();
						Log.i("BufferedReader", "fechou!");
						
					} catch (IOException e) {
						Log.e("br.close(): ", Log.getStackTraceString(e));        
					} 
				}

				conexao.disconnect();
			}

			return tweets;
		}

		@Override
		protected void onPostExecute(ArrayList<Tweet> tweets){
			progressDialog.dismiss();

			if (tweets.isEmpty()) {
				Toast.makeText(getApplicationContext(), "Erro ao carregar os tweets! ",
						Toast.LENGTH_SHORT).show();
			} else {
				popularListView(tweets);
				Toast.makeText(getApplicationContext(), "Tweets carregados!",
						Toast.LENGTH_SHORT).show();
			}
		}


	}


}
