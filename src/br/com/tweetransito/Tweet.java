package br.com.tweetransito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import android.util.Log;

public class Tweet {
	
	private String nome;
	private String usuario;
	private String urlImagemPerfil;
	private String mensagem;
	private String dataEnvio;
	

	public Tweet() {
	}

	/**
	 * @param nome
	 * @param usuario
	 * @param urlImagemPerfil
	 * @param mensagem
	 * @param dataEnvio
	 */
	public Tweet(String nome, String usuario, String url,
			String mensagem, String data) {
		this.nome = nome;
		this.usuario = usuario;
		this.urlImagemPerfil = url;
		this.mensagem = mensagem;
		this.dataEnvio = data;
	}
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	/**
	 * @return the urlImagemPerfil
	 */
	public String getUrlImagemPerfil() {
		return urlImagemPerfil;
	}

	/**
	 * @param urlImagemPerfil the urlImagemPerfil to set
	 */
	public void setUrlImagemPerfil(String url) {
		this.urlImagemPerfil = url;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * @return the dataEnvio
	 */
	public String getData() {
		return dataEnvio;
	}
	
	/**
	 * @param dataEnvio the dataEnvio to set
	 */
	public void setData(String data) {
		this.dataEnvio = formatarData(data.substring(0, 20) + data.substring(26, 30));
	}
	
	private String formatarData(String data){
		String strData = null;
		TimeZone tz = TimeZone.getTimeZone("GMT-3");
		SimpleDateFormat formatoInicial = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.US);
		SimpleDateFormat formatoFinal = new SimpleDateFormat("EEE, dd/MM/yy, 'às' HH:mm");
		
		try {
			formatoFinal.setTimeZone(tz);
			strData = formatoFinal.format(formatoInicial.parse(data));
		} catch (ParseException e) {
			Log.e("parser data", Log.getStackTraceString(e));
		}
		return strData;
	}
	

}
