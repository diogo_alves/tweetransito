package br.com.tweetransito;

import br.com.tweetransito.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity implements OnClickListener{

	private static final String USUARIO = "transitomaceio";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);

		findViewById(R.id.btn_tweets).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {

		switch (view.getId()) {

		case R.id.btn_tweets:
			buscarTweets(USUARIO);
			break;
		}
	} 

	private void buscarTweets(String termo){
		Intent i = new Intent(getApplicationContext(), LeitorTweetsActivity.class);
		i.putExtra("TERMO_DE_BUSCA", termo);
		startActivity(i);
	}
}